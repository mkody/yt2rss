<?php
if (!isset($_GET['code']) && file_exists('feed.xml')) {
    // Make it like we're opening the feed
    header('Content-Type: text/xml');
    $feed = file_get_contents('feed.xml');
    // And replace "kdy.ch/yt2rss/" with the current host and path
    $path = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $feed = str_replace('kdy.ch/yt2rss/', $path, $feed);
    echo $feed;
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>YT2RSS</title>
</head>
<body>
<?php
if (isset($_GET['code'])) {
  echo "<p>Enter this code:</p><code><pre>" . $_GET['code'] . "</pre></code>";
} else {
  echo "<p>This isn't the page you're looking for.</p>";
}
?>
</body>
</html>
