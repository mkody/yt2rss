<?php
$lives = json_decode(file_get_contents('lives.json'), true);
$n = $_GET['name'];
if ($n && array_key_exists($n, $lives)) {
  $id = $lives[$n];
  header('Location: https://www.youtube-nocookie.com/embed/' . $id . '?autoplay=1&mute=1');
} else {
  echo 'No valid "?name="';
}
