<?php
if (PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) die('cli only');
if (!file_exists($file = __DIR__ . '/vendor/autoload.php')) {
  throw new \Exception('please run "composer require google/apiclient:~2.0" in "' . __DIR__ .'"');
}

$credentialsPath = 'oauth2.json';
$outPath = 'public/feed.xml';
if (@$argv[1]) {
  $credentialsPath = 'oauth2_' . $argv[1] . '.json';
  $outPath = 'public/feed_' . $argv[1] . '.xml';
}

require_once __DIR__ . '/vendor/autoload.php';
session_start();

use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Suin\RSSWriter\Item;

/*
 * Set $DEVELOPER_KEY to the "API key" value from the "Access" tab of the
 * Google Developers Console: https://console.developers.google.com/
 * Please ensure that you have enabled the YouTube Data API for your project.
 */
function getClient() {
  global $credentialsPath;

  $client = new Google_Client();
  $client->setApplicationName('yt2rss');
  $client->setScopes('https://www.googleapis.com/auth/youtube.readonly');
  // Set to name/location of your client_secrets.json file.
  $client->setAuthConfig('client_secret.json');
  $client->setAccessType('offline');
  $client->setApprovalPrompt('force');

  if (file_exists($credentialsPath)) {
    $accessToken = json_decode(file_get_contents($credentialsPath), true);
  } else {
    // Request authorization from the user.
    $authUrl = $client->createAuthUrl();
    printf("Open the following link in your browser:\n%s\n", $authUrl);
    print "\nEnter verification code: ";
    $authCode = trim(fgets(STDIN));

    // Exchange authorization code for an access token.
    $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

    // Store the credentials to disk.
    file_put_contents($credentialsPath, json_encode($accessToken));
    printf("Credentials saved to %s\n\n", $credentialsPath);
  }

  $client->setAccessToken($accessToken);

  // Refresh the token if it's expired.
  if ($client->isAccessTokenExpired()) {
    $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
    file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
  }

  return $client;
}

// Define an object that will be used to make all API requests.
$client = getClient();
$service = new Google_Service_YouTube($client);

if (!$client->getAccessToken()) {
  print("no access token, whaawhaaa");
  exit;
}

// Add a property to the resource.
function addPropertyToResource(&$ref, $property, $value) {
  $keys = explode(".", $property);
  $is_array = false;

  foreach ($keys as $key) {
    // For properties that have array values, convert a name like
    // "snippet.tags[]" to snippet.tags, and set a flag to handle
    // the value as an array.
    if (substr($key, -2) == "[]") {
        $key = substr($key, 0, -2);
        $is_array = true;
    }

    $ref = &$ref[$key];
  }

  // Set the property value. Make sure array values are handled properly.
  if ($is_array && $value) {
    $ref = $value;
    $ref = explode(",", $value);
  } elseif ($is_array) {
    $ref = array();
  } else {
    $ref = $value;
  }
}

// Build a resource based on a list of properties given as key-value pairs.
function createResource($properties) {
  $resource = array();

  foreach ($properties as $prop => $value) {
    if ($value) {
      addPropertyToResource($resource, $prop, $value);
    }
  }

  return $resource;
}

// Get all our subs
function listSubs($service, $nextPageToken) {
  try {
    $params = array_filter(
      array(
        'mine' => true,
        'maxResults' => 50,
        'order' => 'unread',
        'pageToken' => $nextPageToken
      )
    );

    $response = $service->subscriptions->listSubscriptions(
      'snippet,contentDetails',
      $params
    );

    return $response;
  } catch (Exception $e) {
    echo "             \nERROR inside listSubs\n",  $e->getMessage(), "\n\n";
    return false;
  }
}

// Get uploaded videos from sub
function uploads($service, $sub) {
  try {
    $listResponse = $service->channels->listChannels(
      'contentDetails',
      array(
        'id' => $sub->snippet->resourceId->channelId
      )
    );

    if (count($listResponse->items) <= 0 || !$listResponse->items[0]->contentDetails->relatedPlaylists->uploads) {
      echo "             \nChannel \"" . $sub->snippet->title . "\" (" . $sub->snippet->resourceId->channelId . ") doesn't have uploads?\n\n";
      return false;
    }

    $response = $service->playlistItems->listPlaylistItems(
      'snippet',
      array(
        'maxResults' => 5,
        'playlistId' => $listResponse->items[0]->contentDetails->relatedPlaylists->uploads
      )
    );

    $items = $response->items;
    $items = array_filter($items, function ($obj) {
      if (strtotime($obj->snippet->publishedAt) > strtotime('-14 days')) return true;
      return false;
    });

    return $items;
  } catch (Exception $e) {
    echo "             \nERROR inside uploads\n",  $e->getMessage(), "\n";
    echo "(channelId: ". $sub->snippet->resourceId->channelId . ")\n\n";
    return false;
  }
}

// Comparator function
function sortByPubDate($a, $b) {
  return strcmp($b->snippet->publishedAt, $a->snippet->publishedAt);
}

echo "Hi! We're going to list all the channels you're subscribed to.\n";

$cost = 0;
$allSubs = [];
$nextPageToken = null;
$i = 1;
$continue = true;
echo "Page " . $i++ . "\r";

while ($continue) {
  $r = listSubs($service, $nextPageToken);
  $cost += 5;

  if ($r) {
    $allSubs = array_merge($allSubs, $r->items);
  }

  if ($r->nextPageToken) {
    $nextPageToken = $r->nextPageToken;
    $tot = floor($r->pageInfo->totalResults / 50) + 1;
    echo "Page " . $i++ . "/" . $tot . "\r";
  } else {
    $continue = false;
  }
}

$allSubs = array_filter($allSubs);

$totalNbr = $r->pageInfo->totalResults;

echo "=> You're subscribed to " . $totalNbr . " channels.\n\n";
echo "Give me between " . floor($totalNbr * 0.15) . " seconds (~" . floor($totalNbr * 0.15 / 60) . " minutes) and " . ceil($totalNbr * 2.5) . " seconds (~" . ceil($totalNbr * 2.5 / 60) . " minutes) to parse them all. ;)\n";
echo "(Note: this run could use up to " . ($cost + ($totalNbr * 6)) . " units in your quota costs!)\n";

$allVids = [];
$i = 1;
foreach ($allSubs as $sub) {
  echo "Sub " . $i++ . "/" . $totalNbr . "\r";
  $uploads = uploads($service, $sub);
  $cost += 6;

  if ($uploads) {
    $allVids = array_merge($allVids, $uploads);
  }
}

$allVids = array_filter($allVids);

echo "=> We now have " . count($allVids) . " videos fetched!\n\n";
echo "Sorting by date and saving the 50 most recent ones...\n";
usort($allVids, "sortByPubDate");
$recent = array_slice($allVids, 0, 50);

$feed = new Feed();

$channel = new Channel();
$channel
  ->title('YouTube Subscriptions Feed')
  ->description('YT2RSS')
  ->url('https://www.youtube.com/feed/subscriptions')
  ->feedUrl('https://kdy.ch/yt2rss/')
  ->language('en-US')
  ->pubDate(strtotime('now'))
  ->lastBuildDate(strtotime('now'))
  ->ttl(30)
  ->appendTo($feed);

foreach($recent as $vid) {
  $url = 'https://www.youtube.com/watch?v=' . $vid->snippet->resourceId->videoId;
  $t = $vid->snippet->thumbnails;
  $imgUrl = isset($t->maxres) ? $t->maxres->url : $t->default->url;
  $img = '<img style="display:block;margin:0 auto;" src="' . $imgUrl . '" />';

  $item = new Item();
  $item
    ->title($vid->snippet->title)
    ->description($img . '<br/><p>' . nl2br($vid->snippet->description) . '</p>')
    ->url($url)
    ->author($vid->snippet->channelTitle)
    ->pubDate(strtotime($vid->snippet->publishedAt))
    ->guid($url, true)
    ->preferCdata(true) // By this, title and description become CDATA wrapped HTML.
    ->appendTo($channel);
}

file_put_contents($outPath, $feed->render());

echo "=> And we're done! feed.xml was made.\n";
echo "We've used " . $cost . " units in your quota to do this.\n";
