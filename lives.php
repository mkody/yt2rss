<?php
if (!file_exists($file = __DIR__ . '/vendor/autoload.php')) {
  throw new \Exception('please run "composer require google/apiclient:~2.0" in "' . __DIR__ .'"');
}
require_once __DIR__ . '/vendor/autoload.php';
session_start();

use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Suin\RSSWriter\Item;

/*
 * Set $DEVELOPER_KEY to the "API key" value from the "Access" tab of the
 * Google Developers Console: https://console.developers.google.com/
 * Please ensure that you have enabled the YouTube Data API for your project.
 */
function getClient() {
  $client = new Google_Client();
  $client->setApplicationName('yt2rss');
  $client->setScopes('https://www.googleapis.com/auth/youtube.readonly');
  // Set to name/location of your client_secrets.json file.
  $client->setAuthConfig('client_secret.json');
  $client->setAccessType('offline');
  $client->setApprovalPrompt('force');

  // Load previously authorized credentials from a file.
  $credentialsPath = 'oauth2.json';

  if (file_exists($credentialsPath)) {
    $accessToken = json_decode(file_get_contents($credentialsPath), true);
  } else {
    // Request authorization from the user.
    $authUrl = $client->createAuthUrl();
    printf("Open the following link in your browser:\n%s\n", $authUrl);
    print "\nEnter verification code: ";
    $authCode = trim(fgets(STDIN));

    // Exchange authorization code for an access token.
    $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

    // Store the credentials to disk.
    file_put_contents($credentialsPath, json_encode($accessToken));
    printf("Credentials saved to %s\n\n", $credentialsPath);
  }

  $client->setAccessToken($accessToken);

  // Refresh the token if it's expired.
  if ($client->isAccessTokenExpired()) {
    $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
    file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
  }

  return $client;
}

// Define an object that will be used to make all API requests.
$client = getClient();
$service = new Google_Service_YouTube($client);

if (!$client->getAccessToken()) {
  print("no access token, whaawhaaa");
  exit;
}

$service = new Google_Service_YouTube($client);

$list = array(
  'euronews_fr' => 'UCW2QcKZiU8aUGg4yxCIditg',
  'skynews' => 'UCoMdktPbSTixAyNGwb-UYkQ',
  'france24' => 'UCCCPCZNChQdGa9EkATeye4g'
);

$res = [];

foreach($list as $c => $i) {
  $queryParams = [
    'channelId' => $i,
    'eventType' => 'live',
    'maxResults' => 1,
    'type' => 'video'
  ];

  $res[$c] = $service->search->listSearch('snippet', $queryParams)['items'][0]['id']['videoId'];
}

file_put_contents("public/lives.json", json_encode($res));
