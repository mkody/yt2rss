# yt2rss
> Absolutly disgusting script to create a single RSS feed with the last 50 videos from your YouTube subscriptions.  
> But at least it's in chronological order and without some sort of filtering.

_It works on my machine, might not in yours._  
If you don't know how to make this run by reading this document or how to use the command line, I'm not going to explain it to you in issues or in private (except if you're a friend).  
But if my instructions below could be improved, please make a merge request.

## Requirements
- Have a webserver with PHP 7 (fpm and cli), the PHP-XML extension and [composer](https://getcomposer.org/download/) installed.
- Run `composer install`.
- Point your webserver to the `public/` folder. (Don't expose the entire project directory!)
- [Create an app](https://console.developers.google.com/projectcreate) in the Google dev console
  - In the side menu, go to "Libraries", search for "YouTube Data API" and enable it.
  - Then [create an Oauth Client for a web application](https://console.developers.google.com/apis/credentials/oauthclient) with the correct redirect url pointing to where you serve the `public/` folder (ie. `https://kdy.ch/yt2rss/`).
  - Once it's done, download the keys from the credentials list, you should get a `client_secret[...].json` file. Rename it to `client_secret.json` and put it next to `fetch.php`.

## Run
### First run
Run `php fetch.php` and follow the instructions to login.  
Script will then run normally.

### Then automatically
Create a cron running every 3 hours (or each hour if you don't have an insane amount of subs; you might have to `cd` to the directory).

```crontab
0 */3 * * * cd /var/www/yt2rss && php fetch.php > cron.log 2>&1
```

## Use
Add the feed to your RSS client using the path to the folder, if you want the `feed url` to point correctly to your current host.

Example: `https://mydomain.com/youtuberss/` (instead of `https://mydomain.com/youtuberss/feed.xml`)

## API consuption
Let's imagine I have 980 subs. On a run, the script could use `5944` units (estimation was `5980`) of the `1 000 000` you're given per day. So I could run this (hourly) ~168 times in 24 hours.
